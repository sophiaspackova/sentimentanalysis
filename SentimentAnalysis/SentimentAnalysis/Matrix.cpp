#include "Matrix.h"
#include <random>

Matrix::Matrix(int numRows, int numCols, bool isRandom)
{
	this->numRows = numRows;
	this->numCols = numCols;


	values = std::vector<std::vector<float> >();
	for (int i = 0; i < numRows; i++)
	{
		std::vector<float> columns = std::vector<float>();
		for (int j = 0; j < numCols; j++)
		{
			columns.push_back(0.0);
			if (isRandom) columns[j] = CreateRandomWeight();
		}
		values.push_back(columns);
	}
}

Matrix* Matrix::Transpose()
{
	Matrix* Transposed = new Matrix(numCols, numRows, false);

	for (int i = 0; i < numCols; i++)
	{
		for (int j = 0; j < numRows; j++)
		{
			Transposed->SetVal(i, j, values[j][i]);
		}
	}

	return Transposed;
}

float Matrix::CreateRandomWeight()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<> dis(0, 1);

	return dis(gen);
}

Matrix* Matrix::Multiply(Matrix* other)
{
	if (numCols != other->numRows) return nullptr;
	Matrix* ToReturn = new Matrix(numRows, other->numCols, false);
	for (int i = 0; i < numRows; i++)
	{
		for (int j = 0; j < other->numCols; j++)
		{
			for (int k = 0; k < other->numRows; k++)
			{
				float p = GetVal(i, k) * other->GetVal(k, j);
				ToReturn->SetVal(i, j, p);
			}
		}
	}

	return ToReturn;
}

std::vector<float> Matrix::ToVector()
{
	std::vector<float> val = std::vector<float>();

	for (int i = 0; i < numRows; i++)
	{
		for (int j = 0; j < numCols; j++)
		{
			val.push_back(GetVal(i, j));
		}
	}

	return val;
}

void Matrix::SetVal(int r, int c, float val)
{
	if (r >= numRows) return;
	if (c >= numCols) return;
	values[r][c] = val;
}

float Matrix::GetVal(int r, int c)
{
	return values[r][c];
}