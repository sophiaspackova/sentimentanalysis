// SentimentAnalysis.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include "Analyzer.h"


int main()
{
	std::string response;

	Analyzer analyze = Analyzer();

	std::cout << "How are you feeling?" << std::endl;
	getline(std::cin, response);

	std::cout << response;
    return 0;
}

