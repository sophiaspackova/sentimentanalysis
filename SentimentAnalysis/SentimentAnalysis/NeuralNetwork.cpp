#include "NeuralNetwork.h"
#include <cmath>
#include <stdlib.h>
#include <ctime>
#include <random>


// Sets default values for this component's properties
ANeuralNetwork::ANeuralNetwork()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	//PrimaryActorTick.bCanEverTick = true;



}

void ANeuralNetwork::StartNeuralNetwork(std::vector<int> topology)
{

	this->topology = topology;

	for (unsigned int i = 0; i < topology.size(); i++)
	{
		Layer* l = new  Layer(topology[i]);
		this->layers.push_back(l);
	}

	for (unsigned int i = 0; i < topology.size() - 1; i++)
	{
		Matrix * m = new Matrix(topology[i], topology[i + 1], true);

		this->weightMatricies.push_back(m);
	}

	int outputLayerIndex = this->layers.size() - 1;
	int sizeOfNeurons = this->layers[outputLayerIndex]->GetNeurons().size();
	for (int i = 0; i < sizeOfNeurons; i++)
	{
		target.push_back(rand() % 2);
		errors.push_back(0.0);
	}
}


// Called when the game starts
/*void ANeuralNetwork::BeginPlay()
{
	//Super::BeginPlay();

	// ...

	std::vector<int> Topology = std::vector<int>();
	//Topology.push_back(AI.size());
	Topology.push_back(4);
	Topology.push_back(4);
	StartNeuralNetwork(Topology);

}

void ANeuralNetwork::Tick(float delta)
{
	GatherInputs();
	FeedForward();
	SetErrors();
	BackPropagation();
	SendOutResult();
} ?*/

void ANeuralNetwork::GatherInputs()
{
	/*for (int i = 0; i < AI.size(); i++)
	{
		empty.EnemyPosition[i] = AI[i]->GetActorLocation();
	} */
}

/*void ANeuralNetwork::GeneticallyModify()
{

	int outputLayerIndex = this->layers.size() - 1;
	int sizeOfNeurons = this->layers[outputLayerIndex]->GetNeurons().size();

	int randIndex = rand() % sizeOfNeurons;

	this->target[randIndex] = this->target[randIndex] / 2.0f;

	if (this->target[randIndex] == 0.0f) this->target[randIndex] = 1.0f;


} */



void ANeuralNetwork::Train(std::string input, Emotion target)
{
	
}

void ANeuralNetwork::SetErrors()
{
	if (this->target.size() == 0)
	{
		return;
	}
	if (this->target.size() != this->layers[this->layers.size() - 1]->GetSize())
	{
		return;
	}

	this->error = 0.0f;

	int outputLayerIndex = this->layers.size() - 1;
	std::vector<Neuron*> outputNeurons = this->layers[outputLayerIndex]->GetNeurons();
	float highestVal = -10000000000000.0f;

	for (unsigned int i = 0; i < target.size(); i++)
	{
		float tempErr = outputNeurons[i]->GetActivatedVal() - target[i];
		if (tempErr > highestVal)
		{
			highestVal = tempErr;
		}
		errors[i] = tempErr;
		this->error += tempErr;
	}

	historicalError.push_back(this->error);
}

void ANeuralNetwork::FeedForward()
{
	for (unsigned int i = 0; i < this->layers.size() - 1; i++)
	{
		Matrix* a = this->GetNeuronMatrix(i);

		if (i != 0)
		{
			a = this->GetActivatedNeuronMatrix(i);
		}

		Matrix* b = this->weightMatricies[i];
		Matrix* c = a->Multiply(b);

		if (c != nullptr)
		{
			for (int c_index = 0; c_index < c->GetNumCols(); c_index++)
			{
				this->SetNeuronVal(i + 1, c_index, c->GetVal(0, c_index));
			}
		}
	}
}

void ANeuralNetwork::BackPropagation()
{
	std::vector<Matrix* > newWeights = std::vector<Matrix*>();
	Matrix* gradients;

	int outputLayerIndex = this->layers.size() - 1;

	Matrix * derivedYToZ = this->layers[outputLayerIndex]->GetDerivedMatrixRepresentation();

	Matrix* gradientsYToZ = new Matrix(1, this->layers[outputLayerIndex]->GetSize(), false);

	for (unsigned int i = 0; i < this->errors.size(); i++)
	{
		float d = derivedYToZ->GetVal(0, i);
		float e = this->errors[i];
		float g = d * e;
		gradientsYToZ->SetVal(0, i, g);
	}

	int lastHiddenLayerIndex = outputLayerIndex - 1;


	Layer* lastHiddenLayer = this->layers[lastHiddenLayerIndex];

	Matrix* weightsOutputToHidden = this->weightMatricies[lastHiddenLayerIndex];

	Matrix* deltaOutputHidden = (gradientsYToZ->Transpose())->Multiply(lastHiddenLayer->GetActivatedMatrixRepresentation());

	Matrix* newWeightsOutputToHidden = new Matrix(deltaOutputHidden->GetNumRows(), deltaOutputHidden->GetNumCols(), false);

	for (int r = 0; r < deltaOutputHidden->GetNumRows(); r++)
	{
		for (int c = 0; c < deltaOutputHidden->GetNumCols(); c++)
		{
			float originalWeights = weightsOutputToHidden->GetVal(r, c);
			float deltaWeight = deltaOutputHidden->GetVal(r, c);
			float newWeight = originalWeights - deltaWeight;
			newWeightsOutputToHidden->SetVal(r, c, newWeight);
		}
	}

	newWeights.push_back(newWeightsOutputToHidden);
	gradients = new Matrix(gradientsYToZ->GetNumRows(), gradientsYToZ->GetNumCols(), false);
	for (int r = 0; r < gradientsYToZ->GetNumRows(); r++)
	{
		for (int c = 0; c < gradientsYToZ->GetNumCols(); c++)
		{
			gradients->SetVal(r, c, gradientsYToZ->GetVal(r, c));
		}
	}

	for (int i = lastHiddenLayerIndex; i > 0; i--)
	{
		Layer* l = this->layers[i];
		Matrix* derivedHidden = l->GetDerivedMatrixRepresentation();
		Matrix* activatedHidden = l->GetActivatedMatrixRepresentation();

		Matrix* derivedGradients = new Matrix(
			1,
			l->GetSize(),
			false
		);

		Matrix* weightMatrix = this->weightMatricies[i];
		Matrix* originalMatrix = this->weightMatricies[i - 1];
		for (int r = 0; r < weightMatrix->GetNumRows(); r++)
		{
			float sum = 0.0f;
			for (int c = 0; c < weightMatrix->GetNumCols(); c++)
			{
				float p = gradients->GetVal(0, c) * weightMatrix->GetVal(r, c);
				sum += p;
			}
			float g = sum * activatedHidden->GetVal(0, r);
			derivedGradients->SetVal(0, r, g);

		}
		Matrix*  leftNeurons = (i - 1) == 0 ? this->layers[i]->GetMatrixRepresentation() : this->layers[i - 1]->GetActivatedMatrixRepresentation();

		Matrix* deltaWeights = (derivedGradients->Transpose())->Multiply(leftNeurons)->Transpose();


		Matrix* newWeightsHidden = new Matrix(
			deltaWeights->GetNumRows(),
			deltaWeights->GetNumCols(),
			false
		);

		for (int r = 0; r < newWeightsHidden->GetNumRows(); r++)
		{
			for (int c = 0; c < newWeightsHidden->GetNumCols(); c++)
			{
				float w = originalMatrix->GetVal(r, c);
				float d = deltaWeights->GetVal(r, c);

				float n = w - d;
				newWeightsHidden->SetVal(r, c, n);
			}
		}

		gradients = new Matrix(derivedGradients->GetNumRows(), derivedGradients->GetNumCols(), false);
		for (int r = 0; r < derivedGradients->GetNumRows(); r++)
		{
			for (int c = 0; c < derivedGradients->GetNumCols(); c++)
			{
				gradients->SetVal(r, c, derivedGradients->GetVal(r, c));
			}
		}

		newWeights.push_back(newWeightsHidden);
	}

	weightMatricies.clear();

	for (int index = newWeights.size() - 1; index >= 0; index--)
	{
		this->weightMatricies.push_back(newWeights[index]);
	}
}


void ANeuralNetwork::SendOutResult()
{
}