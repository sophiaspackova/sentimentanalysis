#include "Analyzer.h"
#include <vector>
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
//#define isDebug

Analyzer::Analyzer()
{
	srand(time(NULL));

	std::ifstream file(filename);
	std::string line = "";
	if (file.is_open())
	{
		while (!file.eof())
		{
			std::getline(file, line);
			ConvertToEmotion(line);

		}

		file.close();
	}
	else
	{
		std::cout << "not open" << std::endl;
	}
}
void Analyzer::ConvertToEmotion(std::string line)
{
	if (line.empty())return;
	std::string emotion = line.substr(0, line.find(","));

	int place = emotion.size() + 1;

	Emotion conveyed = Emotion();
	int isEmotion = line[place] - '0';
	conveyed.Angry = isEmotion;
	place += 2;

#ifdef isDebug
	std::cout << "--------------------------------------------" << std::endl;
	std::cout << "Emotion profile: " << emotion << std::endl;
	if (isEmotion == 1) std::cout << "Angry emotion" << std::endl;
	std::cout << line.substr(place, line.size()) << std::endl;
#endif


	isEmotion = line[place] - '0';
	conveyed.Sad = isEmotion;
	place += 2;

#ifdef isDebug
	if (isEmotion == 1) std::cout << "Sad emotion" << std::endl;
	std::cout << line.substr(place, line.size()) << std::endl;
#endif


	isEmotion = line[place] - '0';
	conveyed.Happy = isEmotion;
	place += 2;


#ifdef isDebug
	if (isEmotion == 1) std::cout << "Happy emotion" << std::endl;
	std::cout << line.substr(place, line.size()) << std::endl;
#endif


	isEmotion = line[place] - '0';
	conveyed.Anxious = isEmotion;
	place += 2;


#ifdef isDebug
	if (isEmotion == 1) std::cout << "Anxious emotion" << std::endl;
	std::cout << line.substr(place, line.size()) << std::endl;
#endif


	isEmotion = line[place] - '0';
	conveyed.Neutral = isEmotion;


#ifdef isDebug
	if (isEmotion == 1) std::cout << "Neutral emotion" << std::endl;
	std::cout << line.substr(place, line.size()) << std::endl;
#endif


	int isTrain = rand() % 10;

	if (isTrain < 6) training[emotion] = conveyed;
	else testing[emotion] = conveyed;
	
	std::cout << "size of training: " << training.size() << std::endl;
	std::cout << "size of testing: " << testing.size() << std::endl;
}

Emotion Analyzer::AnalyzeSentiment(std::string input)
{
	Emotion currSentiment = Emotion();


	return currSentiment;
}

void Analyzer::Train()
{

}