#pragma once

class Neuron
{
public:
	Neuron(float val);

	//Internals
	void Activation();
	void Derive();

	//Setters
	void SetVal(float newVal);

	//Getters
	float GetVal() { return this->val; }
	float GetActivatedVal() { return this->activatedVal; };
	float GetDerivedVal() { return this->derivedVal; }

private:
	float val;

	float activatedVal;

	float derivedVal;
};