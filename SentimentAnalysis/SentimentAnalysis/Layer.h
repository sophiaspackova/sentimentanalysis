#pragma once
#include <vector>
#include "Neuron.h"
#include "Matrix.h"

class Layer
{
public:
	Layer(int size);

	void SetVal(int i, float v);

	Matrix* GetMatrixRepresentation();

	Matrix* GetActivatedMatrixRepresentation();

	Matrix* GetDerivedMatrixRepresentation();

	int GetSize() { return size; };

	std::vector<Neuron*> GetNeurons() { return neurons; }
private:
	int size;

	std::vector<Neuron *> neurons;
};