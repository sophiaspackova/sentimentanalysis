#include "Neuron.h"
#include <cmath>

Neuron::Neuron(float val)
{
	this->val = val;
	Activation();
	Derive();
}

void Neuron::Activation()
{
	activatedVal = val / (1.0 + abs(val));
}

void Neuron::Derive()
{
	derivedVal = activatedVal * (1.0 - activatedVal);
}

void Neuron::SetVal(float newVal)
{
	this->val = newVal;
	Activation();
	Derive();
}