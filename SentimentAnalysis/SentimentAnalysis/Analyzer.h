#pragma once
#include <string>
#include "NeuralNetwork.h"
#include <iostream>
#include <fstream>
#include <string.h>
#include <unordered_map>


//int of emotion in % should add up to 100%
class Analyzer
{
public:
	Analyzer();

	Emotion AnalyzeSentiment(std::string);

	void Train();

	void ConvertToEmotion(std::string line);

private:
	std::string filename = "Sentiment.csv";

	//60- 40 training to testing
	std::unordered_map<std::string, Emotion> training;
	std::unordered_map<std::string, Emotion> testing;


};