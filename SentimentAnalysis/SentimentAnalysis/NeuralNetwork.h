#pragma once

#include <stdint.h>
#include "Matrix.h"
#include "Layer.h"
#include <vector>

struct Emotion
{
	int Angry;
	int Sad;
	int Happy;
	int Anxious;
	int Neutral;
};

class  ANeuralNetwork
{
public:
	// Sets default values for this component's properties
	ANeuralNetwork();
	void StartNeuralNetwork(std::vector<int> topology);

protected:
	// Called when the game starts
	/*virtual void BeginPlay() override;

	virtual void Tick(float delta) override; */
	
	void Train(std::string input, Emotion target);

	void GatherInputs();

	void FeedForward();

	void BackPropagation();

	void SetErrors();

	float GetTotalError() { return this->error; }

	std::vector<float> GetErrors() { return this->errors; }

	void SendOutResult();

	Matrix* GetNeuronMatrix(int index) { return this->layers[index]->GetMatrixRepresentation(); }
	Matrix* GetActivatedNeuronMatrix(int index) { return this->layers[index]->GetActivatedMatrixRepresentation(); }
	Matrix* GetDerivedNeuronMatrix(int index) { return this->layers[index]->GetDerivedMatrixRepresentation(); }

	void SetNeuronVal(int indexLayer, int indexNeuron, float val) { this->layers[indexLayer]->SetVal(indexNeuron, val); }

	std::vector<int> topology;
	std::vector<Layer*> layers;

	//n-1 size of topology
	std::vector<Matrix*> weightMatricies;
	std::vector<Matrix*> gradientMatricies;

	std::vector<float> target;

	//Error
	float error;
	std::vector<float> errors;
	std::vector<float> historicalError;

};
